package com.example.calcbouche;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import static java.lang.Integer.parseInt;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void computeResult(View monBoutton){
        EditText first = (EditText) findViewById(R.id.first);
        EditText second = (EditText) findViewById(R.id.second);

        String firstText = first.getText().toString();
        String secondText = second.getText().toString();

        //do nothing if one of the fields is not filled
        if(firstText.equals("") || secondText.equals("")){
            return;
        }
        int firstValue = parseInt(firstText);
        int secondValue = parseInt(secondText);

        RadioGroup radio = (RadioGroup) findViewById(R.id.radio);
        int selectedOperation = radio.getCheckedRadioButtonId();
        int resultat;
        switch (selectedOperation) {
            case  R.id.radioPlus:
                resultat = firstValue + secondValue;
                break;

            case R.id.radioMoins:
                resultat = firstValue - secondValue;
                break;

            case R.id.radioMultiplie:
                resultat = firstValue * secondValue;
                break;

            case R.id.radioDivise:
                resultat = firstValue / secondValue;
                break;

                default:
                    return;
        }
        TextView resultField = (TextView) findViewById(R.id.resultat);
        resultField.setText(String.valueOf(resultat));
    }

    public void Raz(View monBoutton){
        EditText first = (EditText) findViewById(R.id.first);
        EditText second = (EditText) findViewById(R.id.second);

        first.setText("");
        second.setText("");

        RadioGroup radio = (RadioGroup) findViewById(R.id.radio);
        radio.clearCheck();

        TextView resultField = (TextView) findViewById(R.id.resultat);
        resultField.setText("");
    }

    public void quit(View monBoutton){
        finish();
    }
}
